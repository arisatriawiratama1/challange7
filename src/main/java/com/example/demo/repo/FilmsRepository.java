package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Film;

public interface FilmsRepository extends JpaRepository<Film, Integer>{

}
